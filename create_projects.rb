require 'json'

if ARGV.count != 1
    puts "Expected Arguments"
    puts "Usage: ruby create_projects.rb <path-to-project-file>"
    exit
end

project_dir = ARGV.first
project_name = project_dir.split('/').last
file_path = File.join(Dir.home, '/code/project-files/' + project_name + '.sublime-project')

file_content = {
    "folders":
    [
        {
            "path": project_dir,
            "folder_exclude_patterns": [
                "node_modules",
                ".serverless"
            ]
        }
    ]
}

# open and write to a file with ruby
open(file_path, 'w') { |f|
  f.puts JSON.pretty_generate(file_content)
}

puts "Created project file at #{file_path}"
`subl #{file_path}`
